package test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import app.Calc;
import org.junit.Assert;

class CalcTest {

	@Test
	void testSumMethodExists() {
		try {
			Calc.class.getMethod("Add", String.class);
		} catch (NoSuchMethodException | SecurityException e) {
			fail("No method Add(String) found.");
		}
	}
	
	@Test
	void testEmptyStringReturnsZero() {
		try {
			final int result = new Calc().Add("");
			Assert.assertEquals(0, result);
		} catch (Exception e) {
			fail();
		}
	}
	
	@Test
	void testSumOfOneIsOne() {
		try {
			final int result = new Calc().Add("1");
			Assert.assertEquals(1, result);
		} catch (Exception e) {
			fail();
		}
	}
	
	@Test
	void testOnePlusOneIsTwo() {
		try {
			final int result = new Calc().Add("1,1");
			Assert.assertEquals(2, result);
		} catch (Exception e) {
			fail();
		}
	}
	
	@Test
	void testZeroPlusZeroIsZero() {
		try {
			final int result = new Calc().Add("0,0");
			Assert.assertEquals(0, result);
		} catch (Exception e) {
			fail();
		}
	}
	
	@Test
	void testMinusThreePlusFiveThrowsException() {
		try {
			new Calc().Add("-3,5");
			fail();
		} catch (Exception e) {
			Assert.assertEquals("negatives not allowed: -3", e.getMessage());
		}
	}
	
	@Test
	void testOnePlusTwoPlusThreeIsSix() {
		try {
			final int result = new Calc().Add("1,2,3");
			Assert.assertEquals(6, result);
		} catch (Exception e) {
			fail();
		}
	}
	
	@Test
	void testMinusOnePlusZeroPlusTwoThrowsException() {
		try {
			new Calc().Add("-1,0,2");
			fail();
		} catch (Exception e) {
			Assert.assertEquals("negatives not allowed: -1", e.getMessage());
		}
	}
	
	@Test
	void testOnePlusTwoPlusThreeIsSixDelimitedByNewLine() {
		try {
			final int result = new Calc().Add("1\n2,3");
			Assert.assertEquals(6, result);
		} catch (Exception e) {
			fail();
		}
	}
	
	@Test
	void testNumbersDelimitedBySemicolon() {
		try {
			final int result = new Calc().Add("//;\n1;2");
			Assert.assertEquals(3, result);
		} catch (Exception e) {
			fail();
		}
	}
	
	@Test
	void testMinusOnePlusZeroPlusMinusTwoThrowsException() {
		try {
			new Calc().Add("-1,0,-2");
			fail();
		} catch (Exception e) {
			Assert.assertEquals("negatives not allowed: -1,-2", e.getMessage());
		}
	}
	
	@Test
	void testTwoPlusThousandAndOneIsTwo() {
		try {
			final int result = new Calc().Add("2,1001");
			Assert.assertEquals(2, result);
		} catch (Exception e) {
			fail();
		}
	}
	
	@Test
	void testLongerDelimiter() {
		try {
			final int result = new Calc().Add("//[***]\n1***2***3");
			Assert.assertEquals(6, result);
		} catch (Exception e) {
			fail();
		}
	}
	
	@Test
	void testMultipleCustomDelimiters() {
		try {
			final int result = new Calc().Add("//[*][%]\n1*2%3");
			Assert.assertEquals(6, result);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	void testMultipleLongerDelimiters() {
		try {
			final int result = new Calc().Add("//[::][...][##]\n0...4##5::1");
			Assert.assertEquals(10, result);
		} catch (Exception e) {
			fail();
		}
	}
}
