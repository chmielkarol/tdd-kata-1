package app;

public class Calc {

	public int Add(String text) throws Exception {
		if(text.length() == 0) return 0;
		
		String delimiters = "[,\n]";
		
		//check if different delimiter given
		if(text.startsWith("//")) {
			int delimiterEnd = text.indexOf('\n');
			//Check if writen with square brackets
			if(text.charAt(2) == '[') {
				//Replace square brackets with '|'
				delimiters = text.replaceAll("\\[", "|");
				delimiters = delimiters.substring(3, delimiterEnd);
				delimiters = delimiters.replaceAll("\\]", "");
			}
			else 
				delimiters = text.substring(2, delimiterEnd);

			
			StringBuffer delimitersBuffer = new StringBuffer();
			for(char delimiter : delimiters.toCharArray()) {
				if(delimiter != '|')
					delimitersBuffer.append('\\'); 
				delimitersBuffer.append(delimiter); 
			}
			delimiters = delimitersBuffer.toString();
			
			text = text.substring(delimiterEnd+1);
		}
		
		String[] numbers = text.split(delimiters);
		int sum = 0;
		boolean containsNegative = false;
		String exceptionMessage = "negatives not allowed: ";
		
		for(String numberString : numbers) {
			int number = Integer.valueOf(numberString);
			
			if(number < 0) {
				containsNegative = true;
				exceptionMessage += numberString + ',';
			}
			else {
				if(number<=1000) sum += number;
			}
		}
		
		if(containsNegative) {
			exceptionMessage = exceptionMessage.substring(0, exceptionMessage.length() - 1);
			throw new Exception(exceptionMessage);
		}

		return sum;
	}
	
}
